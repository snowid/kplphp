<?php
## 一般调用方式都写在这里面
//plugin_action('kuaidi/api/kplaction', ['快递100']);

namespace addons\kuaidi\controller;
use addons\kuaidi\model\KuaidiConfig;

class Api
{
    public function initialize()
    {

    }

    # 调用该插件方式
    public function kplaction($apiname='快递100')
    {
        $kuaidi = KuaidiConfig::where(['apiname'=>$apiname])->find();
        $post_data = array();
        $post_data["customer"] = $kuaidi['customer'];
        $key =  $kuaidi['key'];
        $_POST["com"] = 'zhongtong';
        $_POST["num"] = '73127970405168';
        $post_data["param"] = '{"com":"'.$_POST["com"].'","num":"'.$_POST["num"].'"}';
        $url='http://poll.kuaidi100.com/poll/query.do';
        $post_data["sign"] = md5($post_data["param"].$key.$post_data["customer"]);
        $post_data["sign"] = strtoupper($post_data["sign"]);
        $o="";
        foreach ($post_data as $k=>$v)
        {
            $o.= "$k=".urlencode($v)."&";        //默认UTF-8编码格式
        }
        $post_data=substr($o,0,-1);
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST,FALSE);
        curl_setopt($curl, CURLOPT_POST, 1);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $post_data);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        $output = curl_exec($curl);
        curl_close($curl);
        $retuaz=["code"=>1000,"msg"=>"成功","data"=>json_decode($output)];
        return $retuaz;
    }


}